Done -> Mostrar el pasillo en pantalla, con todas las texturas, objetos, etc... (quieto)
Done -> Poner el scroling en marcha
Done -> Hacer aparecer a los malos
Done -> Mirilla (controlada por flechas de dirección o ratón)
Done -> Colisión al golpear (detectar a que enemigo ha golpeado y quitarle vida, o matarlo de un golpe)

Done -> Vida -> muerte -> V0.1, jugable demo

Angel -> Estado elemental

Angel -> Avance de fases basado en conseguir que te maten en el estado elemental correcto -> V0.2, jugable
	Vidas limitadas, como hace falta morir para avanzar, si malgastas vidas con muertes inutiles no puedes acabar el juego

Josemi -> Gestor Storyline -> mostrado al inicio y entre fases, texto y posibles animaciones

Josemi -> Parametrizar Scene

** Semi-extras

Angel -> Sonido -> banda sonara de fondo (quizá por pantalla), sonidos al golpear, morir, etc...

Jacobo -> Niebla de fondo, si hay tiempo

** Extras (cuando todo lo anterior esté acabado)

Obtener vida cada X puntos o tiempo o similar, para que no te quedes bloqueado sin poder acabar el juego
Gamepad -> Pasar los controles a gamepad raton -> joystic, golpear -> boton


Menú 

Diferentes tipos de golpes (que afectan más o menos a diferentes malos)
Otras acciones (como defenderse, esquivar, etc..)
Inventario con objetos que dropean los malos, que pueden ser armas, pociones, etc...
Experiencia para hacerte más fuerte o conseguir ataques y similares

Muchas más fases, con bifurcaciones de caminos, mapa general para poder volver a pantallas anteriores,..
Añadir 'pantallas' salón de la fama, en las que no aparezcan malos, sino una lista de logros de cada personaje que hayas tenido y que habrá muerto para que pudieras pasar de fase con su hijo

Android -> Pasar los controles a golpear donde se indique en la pantalla táctil


