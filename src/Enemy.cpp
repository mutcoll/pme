#include "Enemy.h"
#include <iostream>

using namespace randomize::UI;
using namespace std;

Enemy::Enemy():active(false),Tframe(0.03),tframe(0.0),Tattack(1.0),tattack(0.0), t(0){}

Enemy::Conf::Conf(GLuint texture,
				int frames_X,
				int frames_Y,
				int tam_W,
				int tam_H): tex_0(texture),
		                    frames_x(frames_X),
		                    frames_y(frames_Y),
		                    tam_w(tam_W),
		                    tam_h(tam_H){}

void Enemy::setConfig(Enemy::Conf conf, bool setExplosion)
{
	addCapa(0,conf.tex_0,conf.frames_x,conf.frames_y);
	setTam(conf.tam_w,conf.tam_h);

	this->frame = 0;
	this->numFrames = conf.frames_x * conf.frames_y;
	this->isExplosion = setExplosion;

	setCapa(0, this->frame);
}

int Enemy::CB_Click(Event action, int x, int y)
{
	Element::CB_Click(action, x, y);
	return 0;
}

int Enemy::CB_NoClick()
{
	return 0;
}

Enemy::Type Enemy::onStep( float dt){
	Enemy::Type r = Enemy::NONE;
	if( (this->tframe += dt) >= this->Tframe){
		this->tframe = 0.0;
		this->frame = (this->frame + 1) % this->numFrames;
		setCapa( 0, this->frame);
	}
	if( this->isExplosion == false){
		double dx, dy;
		if( (this->tattack += dt) >= this->Tattack){
			this->tattack = 0.0;
			r = this->type;
		}
		t += dt;
		dx = sin(M_PI * 2 * t * 0.25) * 10;
    		dy = sin(M_PI * 2 * t * 0.33) * 10;
		this->setPos(defaultPosX + dx, defaultPosY + dy);
	}
	if( this->dying)
		if( (this->tDying -= dt) < 0)
			this->active = false;
	return r;
}

bool Enemy::onHit( int x, int y){
	if( this->inside( x, y) ) {
//        this->active = false;
	this->dying = true;
        return true;
    }
    return false;
}


void Enemy::setType(Enemy::Type type) {
    this->type = type;
}

Enemy::Type Enemy::getType() {
    return type;
}
