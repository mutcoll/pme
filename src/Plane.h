/**
 * @file Plane.h
 * @author jmmut 
 * @date 2015-09-04.
 */

#ifndef PME_PLANE_H
#define PME_PLANE_H

#include <vector>
#include <iostream>
#include "UI/Button.h"
#include "graphics/Sprite.h"
#include "graphics/Texture.h"
#include "math/Vector3D.h"

class Plane {
  private:
    float width;
    float separation;
    float depth;
    float posTexture;
    bool orientationHorizontal;
    bool orientationFront;
    bool invertTexture;

    
    int tex;
    float asp;
    float coord[4][2];

  public:
    float x, y, z;


    Plane(float width, float separation, float depth, bool orientationHorizontal) : width(width),
                                                                                    separation(separation),
                                                                                    depth(depth),
                                                                                    orientationHorizontal(orientationHorizontal),
                                                                                    orientationFront(false)
    {
        setPosTexture(0);
        x = y = z = 0;
    }

    Plane() : Plane(5, 5, 5, false) {}

//    void setPosTexture(Vector3D p);
    void setVertical(bool b) { orientationHorizontal = !b;}
    void setHorizontal(bool b) { orientationHorizontal = b;}
    void setFront(bool b) { orientationFront = b;}
    void setInvertTexture(bool b) { invertTexture = b;}

    void setTexture(int t, float aspect) { tex = t; asp = aspect; }
    void setWidth(float f) { width = f; }
    float getWidth() { return width; }
    void setSeparation(float f) {separation = f;}
    void setDepth(float f) { depth = f; }
    void setPos(float _x, float _y, float _z) { x = _x; y = _y; z = _z; }
    void addPos(float _x, float _y, float _z) { x += _x; y += _y; z += _z; }
    void addPosTexture(float dtx) { setPosTexture(posTexture + dtx / 10.0f * asp); }
    void setPosTexture(float px) {
        posTexture = px;
        int v = 0;

        const float d = (depth / 10.0f) * asp;
        coord[v][0] = posTexture;
        coord[v++][1] = 0;

        coord[v][0] = posTexture + d;
        coord[v++][1] = 0;

        coord[v][0] = posTexture + d;
        coord[v++][1] = 1;

        coord[v][0] = posTexture;
        coord[v++][1] = 1;
    }
    void setSpriteTexture(int num_frames_x, int num_frames_y, int frame_x, int frame_y) {
        int v = 0;

        coord[v][0] = (1.0/num_frames_x) * (frame_x);
        coord[v++][1] = (1.0/num_frames_y) * (frame_y);

        coord[v][0] = (1.0/num_frames_x) * (frame_x + 1);
        coord[v++][1] = (1.0/num_frames_y) * (frame_y);

        coord[v][0] = (1.0/num_frames_x) * (frame_x + 1);
        coord[v++][1] = (1.0/num_frames_y) * (frame_y + 1);

        coord[v][0] = (1.0/num_frames_x) * (frame_x);
        coord[v++][1] = (1.0/num_frames_y) * (frame_y + 1);
    }

    void draw() {
        const static GLfloat vertices[] = {
                -0.5,-0.5, 0,
                -0.5, 0.5, 0,
                 0.5, 0.5, 0,
                 0.5,-0.5, 0
        };

        glPushMatrix();
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_TEXTURE_2D);

        if (orientationFront) {

        } else {
            if (!orientationHorizontal) {
                glRotatef(90, 0, 0 ,1);
            }
            glRotatef(90, 1, 0 ,0);
        }

        glTranslatef(x, y, z + separation);
        glScalef(width, depth, 1);

        if (orientationFront) {
            glRotatef(90, 0, 0 ,1);
        }
        if (invertTexture) {
            glRotatef(180, 1, 0, 0);
        }

        glActiveTexture( GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, tex);

        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);

        glVertexPointer(3, GL_FLOAT, 0, vertices);
        glTexCoordPointer(2, GL_FLOAT, 0, coord);

        glDrawArrays ( GL_TRIANGLE_FAN, 0, 4 );

        glDisableClientState(GL_VERTEX_ARRAY);
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);

        glDisable(GL_BLEND);

        glPopMatrix();

    }
};


#endif //PME_PLANE_H
