/**
 * @file UserInterface.h
 * @author jmmut 
 * @date 2015-09-04.
 */

#ifndef PME_USERINTERFACE_H
#define PME_USERINTERFACE_H

#include "UI/Button.h"
#include "TextureLoader.h"
#include "Enemy.h"

using namespace randomize::UI;

class UserInterface {
private:
    Button aim, heart, lastHeart, stateBack, statePoint, stateElements[4];
	Enemy::Conf *enemiesPalete[4], *explosionsPalete[1];
	Enemy enemies[8], explosions[8];
	float life, termica, flotabilidad;

	void drawEnemies();
	void drawLife();
	void drawState();
public:
//    UserInterface();
	void initEnemies();
	void addEnemy();
	bool onStep( float dt);
	void setLife( float l){this->life = l; this->termica = 0.0; this->flotabilidad = 0.0;}
	bool onHit( int x, int y);
	void setHeartPng( const char *path);
	void setStatePng( const char *back, const char *point, const char *elements);
	void killAll(){for(int i=0;i<8;i++)enemies[i].setActive(false);}
	void getState( float &t, float &f){t = termica; f = flotabilidad;}
    void setAimPng(const char * path);
    void setAimPos(float x, float y);
    void draw();

};


#endif //PME_USERINTERFACE_H
