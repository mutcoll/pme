#ifndef PME_ENEMY_H
#define PME_ENEMY_H

#include "UI/Element.h"

using namespace randomize::UI;

class Enemy : public Element
{
public:
    struct Conf {
        Conf(){}
        Conf(GLuint texture,
                int frames_X,
                int frames_Y,
                int tam_W,
                int tam_H);

        GLuint tex_0;	//Textura de fondo
        int frames_x;		//Frames a lo ancho
        int frames_y;		//Frames a lo largo

        int tam_w;	//Tamaño
        int tam_h;
    };
    enum Type {
        AIR = 0, FIRE, EARTH, ICE, NONE
    };
protected:
    bool active,dying,isExplosion;
    int frame, numFrames;
    float Tframe, tframe, Tattack, tattack;
    float defaultPosX, defaultPosY;
    float t,tDying;
    Enemy::Type type;
public:
    Enemy();
    void setConfig(Conf conf, bool setExplosion = false);
    int CB_Click(Event action, int x, int y);
    int CB_NoClick();
    bool isActive(){return this->active;}
    void setActive( bool a){
	this->active = a; this->tattack = 0.0; this->dying = false; this->tDying = 0.25;defaultPosX = pos_x; defaultPosY = pos_y;
	}
	void setExplosion(){ this->isExplosion = true;}
    Enemy::Type onStep( float dt);
    bool onHit( int x, int y);
    void setType(Enemy::Type type);
    Enemy::Type getType();
};


#endif//PME_ENEMY_H
