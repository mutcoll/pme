/**
 * @file UserInterface.cpp
 * @author jmmut 
 * @date 2015-09-04.
 */

#include "UserInterface.h"
#include <iostream>
#include <cstdlib>
using namespace randomize::UI;
using namespace std;
/*
UserInterface::UserInterface() {
    int texIdx = LoadPNG("randomize/pme/img/boton.png");
    cout << "texidx=" << texIdx << endl;
    float width = 5, height = 5;
    Boton::Conf conf(texIdx, 1, 1, 0, 0, 0, 0, width, height);
    aim.setConfig(conf);
    aim.setRotation(180);

}
 */
void UserInterface::setAimPng(const char *path) {

//    int texIdx = LoadPNG(path);
//    float width = 5, height = 5;
//	Button::Conf conf(texIdx, 1, 1, 0, 0, 0, 0, width, height);
//    aim.setConfig(conf);
//    aim.setRotation(180);
}

void UserInterface::draw() {
    BeginDraw();
	this->drawState();
	this->drawLife();
	this->drawEnemies();
//    aim.setRotation(180);
//    aim.Draw();
    EndDraw();
}

void UserInterface::setAimPos(float x, float y) {
//    aim.setPosPixel((x)/10000.0, (y)/10000.0);
    aim.setPosPixel(x, y);
    float px = (getWinWidth()-x)/getWinWidth();
    float py = (getWinHeight()-y)/getWinHeight();
//    aim.setPosTexture(px, py);
}

void UserInterface::initEnemies(){
    int texIdx;
	unsigned int w, h;
    float width = 10, height = 12;
    texIdx = LoadPNG("randomize/pme/img/bicho_aire.png", w, h);
//    this->enemiesPalete[0] = new Enemy::Conf(texIdx, 9, 1, 15 * (w/float(h)/9), 15);
    this->enemiesPalete[0] = new Enemy::Conf(texIdx, 3, 3, 20 * (w/float(h)), 20);
    texIdx = LoadPNG("randomize/pme/img/bicho_fuego.png", w, h);
//    this->enemiesPalete[1] = new Enemy::Conf(texIdx, 9, 1, 15 * (w/float(h)/9), 15);
    this->enemiesPalete[1] = new Enemy::Conf(texIdx, 3, 3, 20 * (w/float(h)), 20);
    texIdx = LoadPNG("randomize/pme/img/bicho_tierra.png", w, h);
//    this->enemiesPalete[2] = new Enemy::Conf(texIdx, 9, 1, 15 * (w/float(h)/9), 15);
    this->enemiesPalete[2] = new Enemy::Conf(texIdx, 3, 3, 20 * (w/float(h)), 20);
    texIdx = LoadPNG("randomize/pme/img/bicho_hielo_fly.png", w, h);
    this->enemiesPalete[3] = new Enemy::Conf(texIdx, 9, 1, 15 * (w/float(h)/9), 15);
    texIdx = LoadPNG("randomize/pme/img/explosion01_0046.png", w, h);
    this->explosionsPalete[0] = new Enemy::Conf(texIdx, 5, 5, 50, 50);
}

void UserInterface::addEnemy(){
	const static float enemiesPos[8][2] = {
		{162.5,125}, {487.5,125}, {812.5,125}, {1137.5,125}, {162.5,475}, {487.5,475}, {812.5,475}, {1137.5,475}
	};
	int r = rand() % 8;
    int enemyType = rand() %4;
	if( this->enemies[r].isActive() == false){
		this->enemies[r].setConfig( this->enemiesPalete[enemyType][0]);
		this->enemies[r].setPosPixel( enemiesPos[r][0], enemiesPos[r][1]);
		this->enemies[r].setType((Enemy::Type) enemyType);
		this->enemies[r].setActive( true);
	}
/*	for(int i=0; i<8; i++){
		if(this->enemies[i].isActive() == false){
			this->enemies[i].setConfig( this->enemiesPalete[0][0]);
			this->enemies[i].setPosPixel( enemiesPos[i][0], enemiesPos[i][1]);
			this->enemies[i].setActive( true);
			break;
		}
	}
*/
}

void UserInterface::drawEnemies(){
	for(int i=0; i<8; i++) {
		if(this->enemies[i].isActive()) {
			this->enemies[i].Draw();
		}
		if( this->explosions[i].isActive())
			this->explosions[i].Draw();
	}
}

bool UserInterface::onStep( float dt){
	static const int reorder[4] = {-1,2,1,2};
	static int reorderI = 0;
	static float TEE = 3.0, TDE = 0.0; // TiempoEntreEnemigos, TiempoDesdeEnemigo
	Enemy::Type type;
	bool r = false;
	if( (TDE += dt) >= TEE){
		TDE = 0.0;
		this->addEnemy();
	}
	for(int i=0; i<8; i++) {
		if( this->explosions[i].isActive())
			this->explosions[i].onStep(dt);
		if(this->enemies[i].isActive()) {
			if( (type = this->enemies[i].onStep(dt)) != Enemy::NONE) {
				if( (this->life -= 1) <= 0) {
					r = true;
				}
//				this->lastHeart.setCapa( 0, reorder[((int)this->life) % 4]);
				this->lastHeart.setCapa( 0, reorder[(reorderI++)%4]);
				switch( type){
				case Enemy::FIRE:
					this->termica++;
					break;
				case Enemy::ICE:
					this->termica--;
					break;
				case Enemy::AIR:
					this->flotabilidad++;
					break;
				case Enemy::EARTH:
					this->flotabilidad--;
					break;
				default:;
				}
			}
		}
	}
	return r;
}

bool UserInterface::onHit( int x, int y){
    bool anyOneDied = false;
	for(int i=0; i<8; i++) {
		if(this->enemies[i].isActive()) {
			if( this->enemies[i].onHit(x,y)){
				anyOneDied = anyOneDied || true;
				this->enemies[i].setExplosion();
				this->explosions[i].setConfig( this->explosionsPalete[0][0], true);
				this->explosions[i].setPosPixel( x,y);
				this->explosions[i].setType((Enemy::Type) Enemy::NONE);
				this->explosions[i].setActive( true);
				this->explosions[i].onHit(x,y);
			}
		}
	}
    return anyOneDied;
}

void UserInterface::setHeartPng(const char *path) {
    int texIdx = LoadPNG(path);
    float width = 5, height = 5;
    Button::Conf conf(texIdx, 2, 2, 0, 1, 1, 1, width, height);
    this->heart.setConfig(conf);
	this->heart.setCapa( 0, 1);
    this->lastHeart.setConfig(conf);
	this->lastHeart.setCapa( 0, 1);
//    aim.setRotation(180);
}

void UserInterface::drawLife(){
	int i;
	for(i=1; i < this->life/4; i++){
		this->heart.setPosPixel( 30*(i-1)+30, 30);
		this->heart.Draw();
	}
	this->lastHeart.setPosPixel( 30*(i-1)+30, 30);
	this->lastHeart.Draw();
}

void UserInterface::drawState(){
	this->stateBack.Draw();
	this->statePoint.setPosPixel( 1200 + this->termica, 60 - this->flotabilidad);
	this->statePoint.Draw();
	for(int i=0; i<4; i++)
		this->stateElements[i].Draw();
}

void UserInterface::setStatePng( const char *back, const char *point, const char *elements){
    int texIdxB = LoadPNG(back), texIdxP = LoadPNG(point), texIdxE = LoadPNG(elements);
    Button::Conf confB(texIdxB, 1,1,0,0,0,0, 15,15);
    Button::Conf confP(texIdxP, 1,1,0,0,0,0, 1,1);
    Button::Conf confE(texIdxE, 2,2,0,0,0,0, 5,5);
    this->stateBack.setConfig(confB);
    this->statePoint.setConfig(confP);
	this->stateBack.setPosPixel( 1200, 60);
	for(int i=0; i<4; i++)
		this->stateElements[i].setConfig(confE);
	this->stateElements[0].setPosPixel( 1200, 20);
	this->stateElements[1].setPosPixel( 1250, 60);
	this->stateElements[2].setPosPixel( 1200, 100);
	this->stateElements[3].setPosPixel( 1150, 60);
	this->stateElements[0].setCapa(0,0);
	this->stateElements[1].setCapa(0,1);
	this->stateElements[2].setCapa(0,3);
	this->stateElements[3].setCapa(0,2);
}


