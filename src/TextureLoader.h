/**
 * @file TextureLoader.h
 * @author jmmut 
 * @date 2015-09-04.
 */

#ifndef PME_TEXTURELOADER_H
#define PME_TEXTURELOADER_H

#include <vector>
#include <iostream>
#include <GL/gl.h>

unsigned int LoadImg(std::vector<unsigned char> image, unsigned width, unsigned height);
unsigned int LoadPNG(const char* filename, unsigned &width, unsigned &height);
unsigned int LoadPNG(const char* filename);


//
//class TextureLoader {
//
//};


#endif //PME_TEXTURELOADER_H
