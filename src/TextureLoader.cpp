/**
 * @file TextureLoader.cpp
 * @author jmmut 
 * @date 2015-09-04.
 */

#include "TextureLoader.h"
#include "graphics/lodepng/lodepng.cpp"

unsigned int LoadPNG(const char* filename) {
    unsigned w, h;
    return LoadPNG(filename, w, h);
}

unsigned int LoadPNG(const char* filename, unsigned &width, unsigned &height) {

    // Load file and decode image.
    std::vector<unsigned char> image;
    unsigned error = lodepng::decode(image, width, height, filename);


    // If there's an error, display it.
    if (error != 0) {
        std::cout << "error loading file " << filename << " . Error(" << error << "): " << lodepng_error_text(error) << std::endl;
        return 0;
    }


    //Invert image
    std::vector<unsigned char> image2(width * height * 4);
    for(size_t y = 0; y < height; y++){
        for(size_t x = 0; x < width; x++){
            for(size_t c = 0; c < 4; c++){
                //aux = image[4 * width * y + 4 * x + c];
                image2[4 * width * y + 4 * x + c] = image[4 * width * (height - y - 1) + 4 * x + c];
                //image[4 * width * (height-y) + 4 * x + c] = aux;
            }
        }
    }

    return LoadImg(image2, width, height);

}

unsigned int LoadImg(std::vector<unsigned char> image, unsigned width, unsigned height) {

    // Enable the texture for OpenGL.
    unsigned int idTextura = 0;
    unsigned int texture_2d = glIsEnabled(GL_TEXTURE_2D);

    glEnable(GL_TEXTURE_2D);
    glGenTextures(1,&idTextura);
    glBindTexture(GL_TEXTURE_2D, idTextura);

    if(!texture_2d){
        glDisable(GL_TEXTURE_2D);
    }
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); //GL_NEAREST = no smoothing
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, 4, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &image[0]);

    return idTextura;

}
