//
// Created by jmmut on 2015-06-09.
//

#ifndef PLOTTER_GESTOR_H
#define PLOTTER_GESTOR_H


#include <string>
#include <stdexcept>
#include "log/log.h"
#include "vgm/Ventana.h"
#include "vgm/ShaderManager.h"
#include "graphics/drawables/Volador.h"
#include "Scene.h"
#include "UserInterface.h"
#include "DialogInterface.h"
#include "miguel/easysound/EasyPlayer.h"


class Gestor: public Ventana
{
public:
    enum Mode {
        DIALOG, GAME
    };

    Gestor(int width = 640, int height = 480);
    void initGL() override;
    void onStep(float) override;
    void onDisplay() override;
    void onKeyPressed(SDL_Event &e) override;
    void onPressed(const Pulsado &p) override;
    void onMouseMotion(SDL_Event &e) override;
    void onMouseButton(SDL_Event &e) override;
    
    string help();

    void setScene(int n) {
	this->sceneOn = n;
    }

    const Mode &getMode() const {
        return mode;
    }

    void setMode(const Mode &mode) {
        Gestor::mode = mode;
    }

	void setDialogs();

private:
    void drawAxes();

    int t;
    float drot, dtrl, mouse_drot, mouse_dtrl;   // delta rotation, delta translation


    Volador vldr;
    Vector3D center;
    bool leftClickPressed;
    bool rightClickPressed;
    bool middleClickPressed;
    Sint32 lastClickX;
    Sint32 lastClickY;

    EasyPlayer easyPlayer;

	int sceneOn, dialogOn;
    Scene scene[2];
    UserInterface dashboard;
    DialogInterface dialogUI[5];

    UI::Element mirilla;
    float mirillaRot = 0;

    Gestor::Mode mode;

    void drawLoadingPage();
};

#endif //PLOTTER_GESTOR_H
