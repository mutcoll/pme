/**
 * @file Scene.cpp
 * @author jmmut 
 * @date 2015-09-04.
 */

#include "Scene.h"

using namespace std;

void Scene::draw() {

//    glBegin(GL_QUADS);
//    glVertex3f(-1.0, 0.0, 0.0);
//    glColor4f(0.5, 1, 0, 0.2);
//    glVertex3f(1.0, 0.0, 0.0);
//    glColor4f(1, 0,
//    glVertex3f(0.0, 1.0, 0.0);
//    glVertex3f(0.0, 1.0, 0.0);
//    glEnd();
	glPushMatrix();

    glTranslatef(0, 1, 0);
    glColor4f(1, 1, 1, 1);
    floor.draw();
    ceiling.draw();
    leftWall.draw();
    rightWall.draw();

    if (use_torchs) {
        for (int i = 0; i < torch_size; ++i) {
            torch[i].draw();
        }
    }


    if (use_objects) {
        for (int i = objects_size; i > 0; --i) {
            objects[(i+ last_object)% objects_size].draw();
        }
    }

    glColor4f(0.5, 0.5, 0.5, 1);
    glPopMatrix();
}

void Scene::setSeparationWidthDepth(float sep, float w, float depth) {
    height = sep;
    width = w;
    this->depth = depth;
}
