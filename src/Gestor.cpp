//
// Created by jmmut on 2015-06-09.
//

#include "Gestor.h"

Gestor::Gestor(int width, int height): Ventana(width, height) {

    SDL_LogSetPriority(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_DEBUG);
    initGL();

    SDL_SetWindowTitle(window, "3D template");
    t = 0;
    randomize::UI::setConfWindow(width, height);
    randomize::UI::setTestBox(false);
    drawLoadingPage();

    setFps(30);

    center = Vector3D(0, 0, 0);
    vldr.setPos(Vector3D(0, 0, -5));
    vldr.setOrientacion(center - vldr.getPos(), Vector3D(0, 1, 0));
    drot = 3;
    dtrl = 0.2;
    mouse_drot = 0.0625f * drot;
    mouse_dtrl = 0.08f * dtrl;
    leftClickPressed = rightClickPressed = false;

    semaforoStep.sumar();
    semaforoDisplay.sumar();

    scene[0].setSeparationWidthDepth(2, 3, 20);
    scene[0].init();
	scene[0].setWalls( "randomize/pme/img/wall_castle_b.png");
	scene[0].setFloor( "randomize/pme/img/suelo.png");
	scene[0].setCeiling( "randomize/pme/img/techo.png");
	scene[0].setTorchs( "randomize/pme/img/torch.png");
    const int frames[][2] = {{1, 0}, {1, 1}};
//    scene[0].setObjects( "randomize/pme/img/objects.png", 2, 2, frames, 2);

    scene[1].setSeparationWidthDepth(2, 3, 20);
    scene[1].init();
	scene[1].setWalls( "randomize/pme/img/puntos.png");
	scene[1].setFloor( "randomize/pme/img/statepoint.png");
	scene[1].setCeiling( "randomize/pme/img/stateback.png");
    const int frames2[][2] = {{0, 0}, {0, 1}};
 //   scene[1].setObjects( "randomize/pme/img/objects.png", 2, 2, frames2, 2);
    dashboard.setAimPng("randomize/pme/img/mirilla.png");
	dashboard.initEnemies();
	dashboard.setLife(40);
	dashboard.setHeartPng("randomize/pme/img/heart.png");
	dashboard.setStatePng("randomize/pme/img/stateback.png","randomize/pme/img/statepoint.png","randomize/pme/img/elementos.png");
    SDL_ShowCursor(0);
    mode = Gestor::DIALOG;
	this->dialogOn = 0;
	this->setDialogs();
	for(int i=0; i<5; i++) {
        dialogUI[i].init(width, height);
    }



    unsigned int mirillaTextId = LoadPNG("randomize/pme/img/mirilla.png");
    mirilla.addCapa(0, mirillaTextId, 1, 1);
    mirilla.setTam(5, 5);
}

void Gestor::setDialogs(){
        std::string text = "A dia de hoy este juego es un juego de esos "        "\n"
                "en los que juegas porque jugando se llega a "              "\n"
                "alguna parte, a no ser que te quedes quieto, "             "\n"
                "que sueles hacerlo cuando juegas, ya que no "              "\n"
                "te mueves de la silla, a no ser que estes "                "\n"
                "jugando con una de esas super plataformas de "             "\n"
                "realidad virtual, aunque, tecnicamente sigues "            "\n"
                "sin moverte del sitio, a no ser que contemos "             "\n"
                "con el movimiento propio de la tierra, el sol, "           "\n"
                "la galaxia... respecto a... respecto a qué? No "           "\n"
                "se sabe, si hubiera un punto \"fijo\" en el "              "\n"
                "universo que pudieramos considerar las coordenadas "       "\n"
                "0,0, podríamos medir ese movimiento" "..." ;
//                " absoluto. "            "\n"
//                "Hasta entonces, el movimiento es relativo...";
	this->dialogUI[0].setDialog(text);
	this->dialogUI[1].setDialog("Por ser la primera pantalla te pondremos\nun requisito fácil de cumplir\nconsigue que te alcance al menos un\ngolpe más de fuego que de hielo.\n");
	this->dialogUI[2].setDialog("Lo conseguiste, enhorabuena\npara la siguiente pantalla te pondremos\nun reto algo mayor, necesitas acumular\nmás hielo que fuego, y más tierra que aire.\n");
	this->dialogUI[3].setDialog("De nuevo lo has logrado\npor desgracia eso es todo por ahora\nesperamos verte en próximas DLC\nmuchas gracias por jugar.\n\n¿Volver al comienzo?");
	this->dialogUI[4].setDialog("GAME OVER.\n¿Otro intento?\n");
}

/** A general OpenGL initialization function.
 * Sets all of the initial parameters.
 * We call this right after our OpenGL window is created.
 */
void Gestor::initGL() {
    GLdouble aspect;
    int width = 640, height = 480;

    if (window == NULL) {
        SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION,
                "There's no window, GL will use (640, 480) in the viewport, and you must create the sdl gl context ");
    } else {
        SDL_GetWindowSize(window, &width, &height);
        context = SDL_GL_CreateContext(window);
        if (!context) {
            SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Unable to create OpenGL context: %s\n", SDL_GetError());
            SDL_Quit();
            exit(2);
        }
    }

    glViewport(0, 0, width, height);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);        // This Will Clear The Background Color To Black
    glClearDepth(1.0);                // Enables Clearing Of The Depth Buffer
    glDepthFunc(GL_LESS);                // The Type Of Depth Test To Do
    glEnable(GL_DEPTH_TEST);            // Enables Depth Testing
    glShadeModel(GL_SMOOTH);            // Enables Smooth Color Shading
    glPointSize(3);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();                // Reset The Projection Matrix

    aspect = (GLdouble)width / height;

    perspectiveGL (45, aspect, 0.1, 5000);

    //glOrtho(-1.0, 1.0, -1.0 / aspect, 1.0 / aspect, 0.0, 1.0);

    glMatrixMode(GL_MODELVIEW);
}

void Gestor::onStep(float dt) {
	float stateX, stateY;
    t++;
    if (mode == Mode::GAME) {
        scene[sceneOn].step(dt);
        if( this->dashboard.onStep(dt)) {
		this->dashboard.getState( stateX, stateY);
		if( sceneOn == 0 && stateX > 0){
			this->dialogOn = 2;
			this->mode = Mode::DIALOG;
			this->sceneOn = 1;
		}else if( sceneOn == 1 && (stateX < 0 && stateY < 0)){
			this->dialogOn = 3;
			this->mode = Mode::DIALOG;
			this->sceneOn = 0;
		}else{
			this->dialogOn = 4;
			this->mode = Mode::DIALOG;
			this->sceneOn = 0;
		}
		this->dashboard.setLife( 40);
		this->dashboard.killAll();
        }
    } else {
        dialogUI[dialogOn].onStep(dt);
    }

    mirilla.setRotation((mirillaRot += dt) * 180);
}

void Gestor::drawAxes() {
    glPushMatrix();
    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(100, 0, 0);

    glColor3f(0, 1, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 100, 0);

    glColor3f(0, 0, 1);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, 100);
    glEnd();

//    glBegin(GL_TRIANGLES);
//    glVertex3f(0, 0, 0);
//    glVertex3f(0, 2, 0);
//    glVertex3f(0, 2, 1);
//    glEnd();

//    glBegin(GL_POLYGON);
//    glColor4f(0, 0.5, 1, 0.2);
//    glVertex3f(-1.0, 0.0, 0.0);
//    glColor4f(0.5, 1, 0, 0.2);
//    glVertex3f(1.0, 0.0, 0.0);
//    glColor4f(1, 0, 0.5, 0.2);
//    glVertex3f(0.0, 1.0, 0.0);
//    glEnd();
    glPopMatrix();
}

void Gestor::onDisplay() {

    // Clear The Screen And The Depth Buffer
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    switch (mode) {
        case DIALOG:
            glLoadIdentity();
            dialogUI[dialogOn].draw();
            break;
        case GAME:
            //glEnable(GL_BLEND);
            //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

            glLoadIdentity();                // Reset The View
            vldr.Look();
            //	glTranslatef( 0, 0, -5.0);

//            drawAxes();
            glPushMatrix();


            scene[sceneOn].draw();
            dashboard.draw();

            glPopMatrix();
            break;
    }

    UI::BeginDraw();
    mirilla.Draw();
    UI::EndDraw();

    // swap buffers to display, since we're double buffered.
    SDL_GL_SwapWindow(window);
}

string Gestor::help() {
    return string("\n\t"
            "h: help:\n\t"
            "space: add one to display and step\n\t"
            "p: play/stop\n\t"
            "w: move arriba\n\t"
            "a: move izquierda\n\t"
            "s: move abajo\n\t"
            "d: move derecha\n\t"
            "q: quit zoom\n\t"
            "e: enter, add zoom\n\t"
            );
}

void Gestor::onPressed(const Pulsado &p) {
    //cout << "p.sym = " << p.sym << endl;	// DEPURACION
    if (p.sym == SDLK_u
        || p.sym == SDLK_o
        || p.sym == SDLK_i
        || p.sym == SDLK_k
        || p.sym == SDLK_j
        || p.sym == SDLK_l
        || p.sym == SDLK_w
        || p.sym == SDLK_s
        || p.sym == SDLK_a
        || p.sym == SDLK_d
        || p.sym == SDLK_q
        || p.sym == SDLK_e) {
        //semaforoStep.cerrar();
    semaforoDisplay.sumar();
//        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    }

    switch(p.sym) {
        case SDLK_u:
            //vldr.setPos(vldr.getPos() + Vector3D(0, dtrl, 0));
                vldr.rotatef(-drot, 0, 0, 1);
            break;
        case SDLK_o:
            //vldr.setPos(vldr.getPos() + Vector3D(0, -dtrl, 0));
                vldr.rotatef(drot, 0, 0, 1);
            break;
        case SDLK_i:
            //vldr.setPos(vldr.getPos() + Vector3D(0, dtrl, 0));
                vldr.rotatef(drot, 1, 0, 0);
                center = vldr.getPos() + vldr.getDir()*(vldr.getPos()-center).Modulo();
            break;
        case SDLK_k:
            //vldr.setPos(vldr.getPos() + Vector3D(0, -dtrl, 0));
                vldr.rotatef(-drot, 1, 0, 0);
                center = vldr.getPos() + vldr.getDir()*(vldr.getPos()-center).Modulo();
            break;
        case SDLK_l:
                vldr.rotY(-drot);
                center = vldr.getPos() + vldr.getDir()*(vldr.getPos()-center).Modulo();
            break;
        case SDLK_j:
                vldr.rotY(drot);
                center = vldr.getPos() + vldr.getDir()*(vldr.getPos()-center).Modulo();
            break;
        case SDLK_e:
            center += vldr.getDir()*dtrl;
            vldr.setPos(vldr.getPos() + vldr.getDir()*dtrl);
            break;
        case SDLK_q:
            center -= vldr.getDir()*dtrl;
            vldr.setPos(vldr.getPos() - vldr.getDir()*dtrl);
            break;
        case SDLK_w:
            center += vldr.getUp()*dtrl;
            vldr.setPos(vldr.getPos() + vldr.getUp()*dtrl);
            break;
        case SDLK_s:
            center -= vldr.getUp()*dtrl;
            vldr.setPos(vldr.getPos() - vldr.getUp()*dtrl);
            break;
        case SDLK_d:
            center -= vldr.getX()*dtrl;
            vldr.setPos(vldr.getPos() - vldr.getX()*dtrl);
            break;
        case SDLK_a:
            center += vldr.getX()*dtrl;
            vldr.setPos(vldr.getPos() + vldr.getX()*dtrl);
            break;
        default:
            break;
    }
}
void Gestor::onKeyPressed(SDL_Event &e) {
    int entero, x, y;

    semaforoDisplay.sumar();
    if (e.type == SDL_KEYUP) {
        return;
    }

    switch(e.key.keysym.sym) {
        case SDLK_h:
            cout << help() << endl;
            break;
        case SDLK_SPACE:
            semaforoDisplay.sumar();
            semaforoStep.sumar();
            break;
        case SDLK_p:	// play / stop
            if (semaforoDisplay.estado())
            {
                semaforoStep.cerrar();
                semaforoDisplay.cerrar();
                semaforoDisplay.sumar();
            }
            else
            {
                semaforoStep.abrir();
                semaforoDisplay.abrir();
            }
            break;
        case SDLK_m:
//            mode = (Mode)(int)!(bool)(int)mode;
            mode = mode == GAME? DIALOG : GAME;
            LOG_DEBUG("changing mode {DIALOG, GAME} to %d\n", mode);
            break;
        case SDLK_z:
            SDL_GetMouseState( &x, &y);
            this->dashboard.onHit(x,y);
            break;
        case SDLK_RIGHT:
            vldr.setPos(vldr.getPos() + Vector3D(-scene[sceneOn].getWidth(), 0, 0));
            break;
        case SDLK_LEFT:
            vldr.setPos(vldr.getPos() + Vector3D(scene[sceneOn].getWidth(), 0, 0));
            break;
        case SDLK_UP:
        case SDLK_DOWN:
        default:
            break;
    }
}

void Gestor::onMouseButton(SDL_Event &e)
{
    lastClickX = e.button.x;
    lastClickY = e.button.y;

    if (mode == DIALOG) {
        switch (dialogUI[dialogOn].onMousePressed(e.button)) {
            case DialogInterface::START_GAME:
                if( this->dialogOn == 0 || this->dialogOn == 3){
                    this->dialogOn = 1;
                }else if(e.type == SDL_MOUSEBUTTONDOWN){
                    mode = GAME;
                }break;
            case DialogInterface::EXIT:
                salir();
                break;
            default:
            case DialogInterface::NONE:
                break;
        };
    } else {

        if (e.button.button == SDL_BUTTON_RIGHT) {
            rightClickPressed = e.button.type == SDL_MOUSEBUTTONDOWN;
        }

        if (e.button.button == SDL_BUTTON_LEFT) {
            leftClickPressed = e.button.type == SDL_MOUSEBUTTONDOWN;
            if (leftClickPressed) {
                if (this->dashboard.onHit(lastClickX, lastClickY)) {
                    easyPlayer.playSound("randomize/pme/sound/dying.wav");
                }
            }
        }

        if (e.button.button == SDL_BUTTON_MIDDLE) {
            middleClickPressed = e.button.type == SDL_MOUSEBUTTONDOWN;
        }

        if (e.button.type == SDL_MOUSEWHEEL) {
            vldr.setPos(vldr.getPos() + e.wheel.y * 10 * vldr.getDir() * dtrl);
        }
    }
    semaforoDisplay.sumar();
}

void Gestor::onMouseMotion(SDL_Event &e)
{
    int w, h;
    SDL_GetWindowSize(window, &w, &h);

    switch (mode) {
        case DIALOG:
            dialogUI[dialogOn].onMouseMotion(e.motion);
            break;
        case GAME:
            if (middleClickPressed) {
                vldr.rotateAround(center, e.motion.yrel*mouse_drot, 1, 0, 0);
                vldr.externalRotateAround(center, -e.motion.xrel*mouse_drot, 0, 1, 0);
            }

            if (rightClickPressed) {
                Vector3D projection_y(vldr.getDir());
                float sense(projection_y.getY()<= 0? 1 : -1);
                projection_y.setY(0);
                Vector3D projection_x(vldr.getX());
                projection_x.setY(0);
                Vector3D translation(e.motion.yrel * mouse_dtrl * projection_y * sense + e.motion.xrel * mouse_dtrl * projection_x);
                center += translation;
                vldr.setPos(vldr.getPos() + translation);
            }

//    dashboard.setAimPos((e.button.x-w/2)/float(w), (e.button.y - h/2)/float(h));
//            dashboard.setAimPos(e.button.x, e.button.y);

            break;
    }
    mirilla.setPosPixel(e.button.x, e.button.y);
    semaforoDisplay.sumar();
}


void Gestor::drawLoadingPage() {

    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    unsigned w, h;
    unsigned int loadingTextId = LoadPNG("randomize/pme/img/loading.png", w, h);
    UI::Element loadingPage;
    loadingPage.addCapa(0, loadingTextId, 1, 1);
    loadingPage.setTamPixel(w, h);

    UI::BeginDraw();
    loadingPage.Draw();
    UI::EndDraw();

    SDL_GL_SwapWindow(window);
}
