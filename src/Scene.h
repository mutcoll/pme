/**
 * @file Scene.h
 * @author jmmut 
 * @date 2015-09-04.
 */

#ifndef PME_SCENE_H
#define PME_SCENE_H

#include <vector>
#include <iostream>
#include "UI/Button.h"
#include "graphics/Sprite.h"
#include "graphics/Texture.h"
#include "Plane.h"
#include "TextureLoader.h"

class Scene {
  private:

    float height, width, depth;
    Plane floor, leftWall, rightWall, ceiling;

    static const int torch_size = 20;
    static const int torch_separation = 20;
    Plane torch[torch_size];
    bool use_torchs = false;

    static const int objects_size = 30;
    static const int objects_separation = 15;

    int last_object;
    Plane objects[objects_size];
    bool use_objects = false;
    int available_objects_size;
    int available_objects[20][2];

    float vel = 10;


  public:

    Scene() : height(10), width(16), depth(10000),
              floor(width, height / 2, depth, true),
              leftWall(height, -width / 2, depth, false),
              rightWall(height, width / 2, depth, false),
              ceiling(width, -height / 2, depth, true)
    {}

	void setWalls( const char *path){
	        unsigned w, h;
	        float asp;
	        int texIdx;
	        texIdx = LoadPNG(path, w, h);
	        asp = float(h) / w;
	        rightWall.setTexture(texIdx, asp);
	        leftWall.setTexture(texIdx, asp);
	}
	void setFloor( const char *path){
	        unsigned w, h;
	        float asp;
	        int texIdx;
	        texIdx = LoadPNG(path, w, h);
	        asp = float(h) / w;
	        floor.setTexture(texIdx, asp);
	}
	void setCeiling( const char *path){
	        unsigned w, h;
	        float asp;
	        int texIdx;
	        texIdx = LoadPNG(path, w, h);
	        asp = float(h) / w;
	        ceiling.setTexture(texIdx, asp);
	}
    void setObjects( const char *path, int frames_w, int frames_h, const int frames_to_use[][2], int num_frames) {
        unsigned w, h;
        float asp;
        int texIdx;
        texIdx = LoadPNG(path, w, h);
        memcpy(available_objects, frames_to_use, num_frames*2 * sizeof(int));
        available_objects_size = num_frames;
        last_object = objects_size - 1;
        use_objects = true;

        const int size = 5;
        for (int i = 0; i < objects_size; ++i) {
            objects[i].setTexture(texIdx, asp);
            objects[i].setFront(true);
            int frame = rand() % num_frames;
            objects[i].setSpriteTexture(2, 2, available_objects[frame][0], available_objects[frame][1]);
            objects[i].setDepth(size);
            objects[i].setWidth(size);
            objects[i].setSeparation(0);
            const bool b = i % 2 == 0;
            objects[i].setInvertTexture(b);
            objects[i].setPos((b ? 1 : -1) * ((rand() % 1000) / 1000.0f) * width, -size/2.0f, i * objects_separation);
        }
    }

    void setTorchs(const char *path) {
        unsigned w, h;
        float asp;
        int texIdx;

        use_torchs = true;
        
        texIdx = LoadPNG(path, w, h);


        for (int i = 0; i < torch_size; ++i) {
            torch[i].setTexture(texIdx, asp);
            torch[i].setFront(true);
            torch[i].setSpriteTexture(1, 1, 0, 0);
            torch[i].setDepth(2);
            torch[i].setWidth(2);
            torch[i].setSeparation(0);
            const bool b = i % 2 == 0;
            torch[i].setInvertTexture(b);
            torch[i].setPos((b ? 1 : -1) * width * 2.3f, height, i * torch_separation);
        }
    }
    
    void init() {
        
    }

    void setSeparationWidthDepth(float height, float width, float depth);

    float getWidth() { return width; }
    float setVelocity(float v) { vel = v; }
    float setUsesTorchs(bool b) {use_torchs = b;}
    void draw();

    void step(float dt) {

        floor.addPosTexture(dt * vel);
        ceiling.addPosTexture(dt * vel);
        rightWall.addPosTexture(dt * vel);
        leftWall.addPosTexture(dt * vel);


        if (use_torchs) {
            for(int i = 0; i < torch_size; i++) {
                torch[i].addPos(0, 0, -dt * vel);
                if (torch[i].z < -3) {
                    torch[i].addPos(0, 0, torch_separation*torch_size);
                }
            }
        }
        if (use_objects) {
            for(int i = 0; i < objects_size; i++) {
                objects[i].addPos(0, 0, -dt * vel);
                if (objects[i].z < -3) {
                    last_object = i;
                    objects[i].addPos(0, 0, objects_separation * objects_size);
                }
            }
        }
    }

    bool collision(int x, int y) {
        if (use_torchs) {
            for(int i = 0; i < torch_size; i++) {
                if ( torch[i].z < 1  && torch[i].z > -1) {

                }
            }
        }
    }


};


#endif //PME_SCENE_H
