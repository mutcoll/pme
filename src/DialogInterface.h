/**
 * @file DialogInterface.h
 * @author jmmut 
 * @date 2015-09-05.
 */

#ifndef PME_DIALOGINTERFACE_H
#define PME_DIALOGINTERFACE_H

#include <randomize/common-libs/src/UI/Button.h>
#include <latinga/sdl2/include/SDL_events.h>
#include <iostream>
#include <randomize/common-libs/src/graphics/SpriteChar.h>
#include "UI/Panel.h"
#include "UI/Text.h"
#include "TextureLoader.h"

#include <string>
#include <sstream>
#include <vector>


using namespace std;
using namespace randomize;

std::vector<std::string> & split(const std::string &s, char delim, std::vector<std::string> &elems);
std::vector<std::string> split(const std::string &s, char delim);


namespace randomize { namespace UI {

class MyText : public Text {
  public:
    void align(int x) {
        setPos(x + tam_w / 2, pos_y);
    }

};

};};


class DialogInterface {
private:
	std::string text;
public:

    enum ButtonPressed {
        NONE = 0,
        START_GAME = 1,
        EXIT = 2
    };


    void onStep(float dt) {
//        aim.setRotation((t += dt) * 180);
    }

    void draw() {
        UI::BeginDraw();
        panel.Draw();
//        aim.Draw();
        UI::EndDraw();
    }

	void setDialog( const std::string &str){
		this->text = str;
	}

    void init(int width, int height) {
        unsigned int botonTextId = LoadPNG("randomize/pme/img/boton.png");
        unsigned int mirillaTextId = LoadPNG("randomize/pme/img/mirilla.png");
        unsigned int textTextId = LoadPNG("randomize/pme/img/letras.png");
        unsigned int backgroundTextId = LoadPNG("randomize/pme/img/wall_castle_b.png");
        UI::Element *background = new UI::Element();
        panel.addElemento(background, NONE);
        background->addCapa(0, backgroundTextId, 1, 1);
        background->setTamPixel(width, height);

        SpriteChar::setTexture(textTextId);
        UI::Button::Conf confBoton = {botonTextId, 1, 5, 0, 0, 0, 1, 20, 5};
        UI::Button::Conf confPuntero = {mirillaTextId, 1, 1, 0, 0, 0, 0, 5, 5};

        UI::Boton_Text *const button = new UI::Boton_Text();
        UI::Boton_Text *const button2 = new UI::Boton_Text();
        UI::Boton_Text *const button3 = new UI::Boton_Text();

        panel.addElemento(button, START_GAME);
        button->setPos(-20, 10);
        button->setConfig(confBoton);
        button->setTamLetra(3);
        button->setFrase("Muere!");

        panel.addElemento(button2, 0);
        button2->setPos(-20, 0);
        button2->setConfig(confBoton);
        button2->setTamLetra(3);
        button2->setFrase("Creditos");
        string creditos = "TODO: Add creditos";
        UI::MyText *const creditos_text = new UI::MyText;
        creditos_text->setPos(1000,1000);
        creditos_text->setTamAuto(true);
        creditos_text->setFrase(creditos.c_str());
        creditos_text->setTamLetra(5);
        creditos_text->setColor3f(1, 0, 0);
        creditos_text->align(20);
        panel.addElemento(creditos_text, 0);

        button2->setListener([creditos_text](UI::Event event, int x, int y, UI::Element *element, void *) {
            if (event == UI::Event::UI_EVENT_DOWN) {
                creditos_text->setPos(20, 20);
            } else if (event == UI::Event::UI_EVENT_UP) {
                creditos_text->setPos(1000, 1000);
            }
        }, nullptr);

        panel.addElemento(button3, EXIT);
        button3->setPos(-20, -10);
        button3->setConfig(confBoton);
        button3->setTamLetra(3);
        button3->setFrase("Salir");
//        aim.setConfig(confPuntero);

        const vector<string> &lines = split(text, '\n');

        int numLines = 0;
        for (const string line : lines) {
            UI::MyText *const text = new UI::MyText;
            text->setTamAuto(true);
            text->setFrase(line.c_str());
            text->setTamLetra(3);
            text->setPos(0, 20 + numLines++ * -4);
            text->align(0);
            panel.addElemento(text, 0);

        }

    }

    void reset() {

    }


    void onMouseMotion(SDL_MouseMotionEvent& event) {
        panel.CB_Click(UI::Event::UI_EVENT_MOVE, event.x, event.y);
//        aim.setPosPixel(event.x, event.y);
    }

    ButtonPressed onMousePressed(SDL_MouseButtonEvent& event) {
        if (event.type == SDL_MOUSEBUTTONDOWN) {
            return (ButtonPressed) panel.CB_Click(UI::Event::UI_EVENT_DOWN, event.x, event.y);
        } else {
            return (ButtonPressed) panel.CB_Click(UI::Event::UI_EVENT_UP, event.x, event.y);
        }
    }

  private:
    UI::Panel<50> panel;
//    UI::Button aim;
    float t = 0;

};




#endif //PME_DIALOGINTERFACE_H
