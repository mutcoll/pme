/**
 * @file main.cpp
 */

#include "utils/SignalHandler.h"
#include "Gestor.h"

using namespace std;

int main(int argc, char **argv)
{
    // this prints in cout the stack trace if there is a segfault. try dereferencing a null in an inner function
    SigsegvPrinter::activate(cout);

    Gestor gestor(1300, 700);

    if (argc > 1) {
        cout << gestor.help();
    }
    gestor.mainLoop();

    cout << endl;
    return 0;
}
