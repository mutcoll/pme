
# Pasillo de Muertes Elementales

Development in progress

## To test

```
bii init pme -l clion
cd pme
bii setup:cpp
open randomize/pme
bii configure -DCMAKE_BUILD_TYPE=RELWITHDEBINFO
bii build
cd bin
./randomize_pme_src_main
```

## To start developing

```
bii init pme -l clion
cd pme
bii setup:cpp
mkdir -p blocks/randomize
cd blocks/randomize
git clone git@bitbucket.org:mutcoll/pme.git
cd pme
bii configure -DCMAKE_BUILD_TYPE=RELWITHDEBINFO
bii build
cd ../../../bin
./randomize_pme_src_main
```

# Powered by

* SDL
* OpenGL
* lodePng
* biicode
* Audio Engine supplied by FMOD by Firelight Technologies
* randomize common-libs

